const fs = require('fs');
const express = require('express')();

express.get('/', (_req, res) => {
    const f = fs.readdirSync('/storage');
    res.status(200).json({
        res: true,
        data: f
    })
});

const PORT = process.env.NODE_PORT || 8090;

express.listen(PORT, () => {
    console.log('started '+PORT);
});