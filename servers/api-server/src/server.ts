import express from 'express';
import rateLimit = require('express-rate-limit');
import helmet = require('helmet');
import http = require('http');
import cors = require('cors');

import * as rootRouter from './routes/root';
import fileUpload = require('express-fileupload');

const LIMITER_MAX = parseInt(process.env.LIMITER_MAX || '50');
const LIMITER_WINDOW_MS = parseInt(process.env.LIMITER_WINDOW_MS || `${15 * 60 * 1000}`)

export const app = express();
const limiter = rateLimit({
    windowMs: LIMITER_WINDOW_MS,
    max: LIMITER_MAX
});

app.use(helmet());
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.disable('x-powered-by');
app.disable('etag');
if(process.env.NODE_ENV === 'production')
    app.use('/', limiter);
app.use(fileUpload());
app.use('/', rootRouter.router);

app.use((_req: express.Request, res: express.Response) => {
    res.status(404).json({
        res: false,
        reason: 'method not found'
    });
});

const httpServer = http.createServer(app);

const server = httpServer.listen(8031, '127.0.0.1', () => console.info('HTTP server started'));
server.on('SIGINT', () => {
    console.warn('HTTP server closing');
    server.close();
});