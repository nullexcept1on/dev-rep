import * as express from "express";
import { library } from "../utils/library";

export const LIBRARY = new library();

const router = express.Router();

router.get('/ping', (_req, res) => res.status(200).json({res: true}));

router.get('/category', (_req, res) => {
    LIBRARY.listCategory('')
        .then((d: string[]) => {
            res.status(200).json({
                res: true,
                data: d
            });
        })
        .catch((s: string) => {
            res.status(500).json({
                res: false,
                reason: s
            });
        });
});

router.get('/category/*', (req, res) => {
    LIBRARY.listCategory(req.params[0])
        .then((d: string[]) => {
            res.status(200).json({
                res: true,
                data: d
            });
        })
        .catch((s: string) => {
            res.status(500).json({
                res: false,
                reason: s
            });
        });
});

router.put('/category/*/:category', (req, res) => {
    if(!req.params['category']) {
        res.status(400).json({
            res: false,
            reason: '/category/:category'
        });
        return;
    }

    LIBRARY.addCategory(req.params[0]+'/'+req.params['category'])
        .then(() => {
            res.status(200).json({
                res: true
            });
        })
        .catch((s: string) => {
            res.status(500).json({
                res: false,
                reason: s
            });
        });
});

router.put('/file', (req, res) => {
    if(!req.files) {
        res.status(400).json({
            res: false,
            reason: 'no data'
        });
        return;
    }

    const f: any = req.files.data;
    LIBRARY.addFiles(f, '')
        .then(() => {
            res.status(200).json({
                res: true
            });
        })
        .catch(() => {
            res.status(500).json({
                res: false
            });
        })
});

router.put('/file/*/:category', (req, res) => {
    if(!req.files) {
        res.status(400).json({
            res: false,
            reason: 'no data'
        });
        return;
    }

    const f: any = req.files.data;

    LIBRARY.addFiles(f, req.params[0]+'/'+req.params['category'])
        .then(() => {
            res.status(200).json({
                res: true
            });
        })
        .catch(() => {
            res.status(500).json({
                res: false
            });
        });
});

router.put('/file/:category', (req, res) => {
    if(!req.files) {
        res.status(400).json({
            res: false,
            reason: 'no data'
        });
        return;
    }

    const f: any = req.files.data;

    LIBRARY.addFiles(f, req.params['category'])
        .then(() => {
            res.status(200).json({
                res: true
            });
        })
        .catch(() => {
            res.status(500).json({
                res: false
            });
        });
});

router.delete('/category/:category', (req, res) => {
    if(!req.params['category']) {
        res.status(400).json({
            res: false,
            reason: '/category/:category'
        });
        return;
    }

    LIBRARY.removeCategory(req.params['category'])
        .then((f: boolean) => {
            res.status(f?200:400).json({
                res: f
            });
        })
        .catch(() => {
            res.status(500).json({
                res: false
            });
        });
});

router.delete('/file/:category/:filename', (req, res) => {
    if(!req.params['category'] || !req.params['filename']) {
        res.status(400).json({
            res: false,
            reason: '/file/:category/:filename'
        });
        return;
    }
    LIBRARY.removeFile(req.params['filename'], req.params['category'])
        .then(() => {
            res.status(200).json({
                res: true
            });
        })
        .catch(() => {
            res.status(500).json({
                res: false
            });
        });
});

router.delete('/file/:filename', (req, res) => {
    if(!req.params['filename']) {
        res.status(400).json({
            res: false,
            reason: '/file/:filename'
        });
        return;
    }
    LIBRARY.removeFile(req.params['filename'], '')
        .then(() => {
            res.status(200).json({
                res: true
            });
        })
        .catch(() => {
            res.status(500).json({
                res: false
            });
        });
});

export {router}