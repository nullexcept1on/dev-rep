import fs = require('fs');

// const ROOT_DIR = './storage';
const ROOT_DIR = '/srv/file-storage';

function getFilesizeInMB(filename: string) {
    var stats = fs.statSync(filename)
    return stats["size"] / 1000000.0;
}

export class library {
    listCategory(name: string): Promise<any[]> {
        return new Promise((res, rej) => {
            const path = name?`${ROOT_DIR}/${name}`:ROOT_DIR;
            if(fs.existsSync(path)) {
                const files = fs.readdirSync(path);
                if(files.length === 0) {
                    res([]);
                    return;
                }
                let data = [];
                for(let i = 0, l = files.length; i < l; i++) {
                    if(fs.lstatSync(`${path}/${files[i]}`).isDirectory())
                        data.push({
                            obj: files[i],
                            type: 1
                        });
                    else
                        data.push({
                            obj: files[i],
                            size: getFilesizeInMB(`${path}/${files[i]}`),
                            type: 0
                        });
                }
                res(data.sort((a, b) => a['type'] > b['type'] ? -1 : 1));
                return;
            }
            rej('category not found');
        });
    }

    addCategory(name: string): Promise<string> {
        return new Promise((res, rej) => {
            if(fs.existsSync(`${ROOT_DIR}/${name}`)) {
                if(!fs.lstatSync(`${ROOT_DIR}/${name}`).isDirectory()) {
                    rej('is exists, but this file');
                    return;
                }
                rej('category is exists');
                return;
            }

            try {
                fs.mkdirSync(`${ROOT_DIR}/${name}`);
            } catch(e) {
                console.error(e);
                rej('unknwn err');
                return;
            }
            res();
        });
    }

    removeCategory(path: string): Promise<boolean> {
        return new Promise((res, rej) => {
            if(!fs.lstatSync(`${ROOT_DIR}/${path}`).isDirectory()) {
                res(false);
                return;
            }
            try {
                fs.rmdirSync(`${ROOT_DIR}/${path}`, {
                    recursive: true
                });
            } catch(e) {
                console.error(e);
                rej();
                return;
            }
            res(true);
        });
    }

    removeFile(file: string, category: string): Promise<void> {
        return new Promise((res, rej) => {
            if(!category)
                category = '';
            else {
                category = category + '/';
                if(!fs.existsSync(`${ROOT_DIR}/${category}`)) {
                    rej();
                    return;
                }
            }

            if(!fs.existsSync(`${ROOT_DIR}/${category}${file}`)) {
                rej();
                return;
            }

            try {
                fs.unlink(`${ROOT_DIR}/${category}${file}`, ()=>{});
            } catch {
                rej();
                return;
            }
            res();
        });
    }

    addFiles(files: any, category: string): Promise<void> {
        if(category) {
            if(!fs.existsSync(ROOT_DIR+'/'+category)) {
                console.log('Create dir: '+category);
                fs.mkdirSync(ROOT_DIR+'/'+category);
            }
            category = category + '/';
        } else {
            category = '';
        }
        return new Promise((res, rej) => {
            if(Array.isArray(files)) {
                const f: any[] = [];

                for(let i = 0, l = files.length; i < l; i++) {
                    f.push({
                        name: files[i].name,
                        md5: files[i].md5,
                        data: files[i].data
                    });
                }

                const filteredFiles = f.reduce((acc: any, current: any) => {
                    const x = acc.find((item: any) => item.md5 === current.md5);
                    if (!x)
                        return acc.concat([current]);
                    else
                        return acc;
                }, []);

                let fd: any[] = [];
                filteredFiles.forEach((v: any) => {
                    fd.push({
                        name: v.name,
                        data: v.data
                    });
                    delete v.data;
                });

                try {
                    fd.forEach(f => {
                        fs.writeFile(ROOT_DIR+`/${category}${f.name}`, f.data, ()=>{
                            console.log(`Add file: ${ROOT_DIR}/${category}${f.name}`);
                        });
                    });
                } catch(e) {
                    console.error(e);
                    rej();
                    return;
                }
            } else {
                try {
                    fs.writeFile(`${ROOT_DIR}/${category}${files.name}`, files.data, ()=>{
                        console.log(`Add file: ${ROOT_DIR}/${category}${files.name}`);
                    });
                } catch(e) {
                    console.error(e);
                    rej();
                    return;
                }
            }
            res();
        });
    }
}