import React from 'react';
import iconFolder from './svg/icons/folder.svg';
import iconFileLog from './svg/icons/log.svg';
import iconFilePdf from './svg/icons/pdf.svg';
import iconFileEmpty from './svg/icons/empty.svg';

import './scss/App.scss';
import './scss/files.scss';

let currentFolder = '';
const ADDRESS = 'http://127.0.0.1:8031';
const ADDRESS_NGINX = 'http://127.0.0.1'

interface IFile {
  obj: string;
  size: number;
  type: number;
}

function ext2svg(ext: string) {
  switch(ext) {
    case 'log':
      return iconFileLog;
    case 'pdf':
      return iconFilePdf;
    case 'json':
      return iconFileEmpty;
    default: 
      return iconFileEmpty;
  }
}

function FileSystem(moveToPath: string = ''): Promise<Array<{obj: string, type: number}>> {
  return new Promise((res, rej) => {
    if(moveToPath) {
      let paths = currentFolder.split('/');
      switch(moveToPath) {
        case '..':
          paths.pop();
          break;
        case '.':
          break;
        default:
          paths.push(moveToPath);
      }
      currentFolder = paths.join('/');
    } else {
      currentFolder = '';
    }
    fetch(`${ADDRESS}/category${currentFolder}`)
      .then(r => {return r.json()})
      .then(d => {
        res(d);
        return;
      })
      .catch(() => rej());
  });
}

export class App extends React.Component<{}, {files: Array<IFile>}> {
  constructor(p: any) {
    super(p);
    this.state = {
      files: []
    }
  }

  handleSubmit(acceptedFiles: FileList | null) {
    const data = new FormData();
    if(acceptedFiles === null)
      return;
    for (const file of acceptedFiles) {
      data.append('data', file, file.name);
    }
    
    fetch(`${ADDRESS}/file${currentFolder}`, {method: 'PUT', body: data})
      .then(() => {
        FileSystem('.')
          .then((d: any) => {
            this.setState({
              files: d['data'] || []
            });
          })
      });
  }

  componentDidMount() {
    const el = document.getElementById('uploadbanner') as HTMLFormElement;
    const el2 = document.getElementById('fileupload') as HTMLInputElement;

    el.onsubmit = (e) => {
      e.preventDefault();
      console.info(el2.files);
      this.handleSubmit(el2.files);
    }
  
    FileSystem()
      .then((d: any) => {
        this.setState({
          files: d['data'] || []
        });
      });
  }
  
  render() {
    return (
      <div className="container">
        <form id="uploadbanner" encType="multipart/form-data">
          <input id="fileupload" name="data" type="file" multiple />
          <input type="submit" value="submit" id="submit" />
        </form>
        <p>Current folder: .{currentFolder}</p>
        <ControlButtons component={this}/>
        <div className="filelist">
          <ul id="files">
            {this.state.files.reverse().map((v, i) => (
              <File key={i} component={this} i={i} isDir={v.type} name={v.obj} size={v.size}/>
            ))}
          </ul>
        </div>
      </div>
    )
  }
}

const File = (props: {
  isDir: number;
  component: React.Component;
  i: number;
  name: string;
  size: number;
}) => {
  if(props.isDir) {
    return (
      <li key={props.i} onClick={() => {
        FileSystem(props.name)
          .then((d: any) => {
            props.component.setState({
              files: d['data'] || []
            });
          });
      }}>
        <a>
          <img className="icon" src={iconFolder} alt="i"/>
          <span>{props.name}</span>
        </a>
      </li>
    )
  }
  return (
    <li key={props.i}>
      <a href={`${ADDRESS_NGINX}/library/`+props.name}>
        <img className="icon" src={ext2svg(props.name.split('.').slice(-1)[0])} alt="i"/>
        <span>{props.name} ({Math.round((props.size)*100)/100} MB)</span>
      </a>
    </li>
  )
}

const ControlButtons = (props: {component: React.Component}) => {
  if(currentFolder) {
    return (
      <div className="filelist">
        <ul>
          <li onClick={()=>{
            FileSystem('..')
              .then((d: any) => {
                props.component.setState({
                  files: d['data'] || []
                });
              });
          }}><a><span
          // style={{
          //   margin: '0 auto',
          //   marginTop: '10px'
          // }}
          >../</span></a></li>
        </ul>
      </div>
    )
  }
  return (<></>);
}